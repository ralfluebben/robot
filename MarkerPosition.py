#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cv2
import logging
import traceback

class MarkerPosition:
    """ Represents the position of a detected marker. It stores the coordinates in camera and world coordinate system.
    The position estimates are filtered by a Kalman filter, which currently does not show any significant improvement, but may be improved
    by better design of measurement and process noise matrix.
    """
    def __init__(self,t,r,idx,log_level=logging.INFO):
        """ Initialzes the object, initial positions are initialzed with [0,0,0] """
        self.log_level=log_level
        self.id=idx
        self.initLogger()
        self.logger.debug("New Marker Position with id {0}".format(idx))
        self.initFilter()
        self.tvec_cam=t
        self.rvec_cam=r
        self.tvec_cam_filter=t
        self.rvec_cam_filter=r
        self.tvec_world=[0,0,0]
        self.rvec_world=[0,0,0]
        self.tvec_world_filter=[0,0,0]
        self.rvec_world_filter=[0,0,0]
        self.not_detected=0

    def update(self,t,r):
        """ Updates the Kalman filter. """
        self.tvec_cam=t
        self.rvec_cam=r

        self.kalman_tvec.correct(np.array(self.tvec_cam,np.float32))
        self.kalman_rvec.correct(np.array(self.rvec_cam,np.float32))

        self.tvec_cam_filter=self.kalman_tvec.predict().transpose()[0].tolist()
        self.rvec_cam_filter=self.kalman_rvec.predict().transpose()[0].tolist()

        self.logger.debug("self.tvec_cam: {0}".format(self.tvec_cam))
        self.logger.debug("self.rvec_cam: {0}".format(self.rvec_cam))
        self.logger.debug("predicted: self.tvec_cam_filter: {0}".format(self.tvec_cam_filter))
        self.logger.debug("predicted: self.rvec_cam_filter: {0}".format(self.rvec_cam_filter))

    def initLogger(self):
        """ Initializes the logger. """
        # from https://docs.python.org/2/howto/logging.html#configuring-logging
        loggername="MarkerPosition_" + str(self.id)
        self.logger=logging.getLogger(loggername)
        self.logger.setLevel(self.log_level)
        ch = logging.StreamHandler()
        ch.setLevel(self.log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(funcName)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def initFilter(self):
        """ Initalizes the measurement and process noise matrix of the Kalman filter. """
        # good example at http://nbviewer.jupyter.org/github/balzer82/Kalman/blob/master/Kalman-Filter-CA-Ball.ipynb?create=1
        tm1=np.array([[1,0,0],
                    [0,1,0],
                    [0,0,1]],dtype=np.float32)
        tm2=np.array([[1,0,0],
                    [0,1,0],
                    [0,0,1]],dtype=np.float32)

        pn1=np.array([[1,1,1],
                    [1,1,1],
                    [1,1,1]],dtype=np.float32)*0.01
        pn2=np.array([[1,1,1],
                    [1,1,1],
                    [1,1,1]],dtype=np.float32)*0.01

        #mn1=np.array([[1,1,1],
        #            [1,1,1],
        #            [1,1,1]],dtype=np.float32)*0.03
        #mn2=np.array([[1,1,1],
        #            [1,1,1],
        #            [1,1,1]],dtype=np.float32)*0.03

        self.kalman_tvec = cv2.KalmanFilter(3,3)
        self.kalman_tvec.measurementMatrix = np.array([[1,0,0],[0,1,0],[0,0,1]],np.float32)
        self.kalman_tvec.transitionMatrix = tm1
        self.kalman_tvec.measurementNoiseCov = pn1
        #self.kalman_tvec.processNoiseCov = mn1

        self.kalman_rvec = cv2.KalmanFilter(3,3)
        self.kalman_rvec.measurementMatrix = np.array([[1,0,0],[0,1,0],[0,0,1]],np.float32)
        self.kalman_rvec.transitionMatrix = tm2
        self.kalman_rvec.measurementNoiseCov = pn2
        #self.kalman_tvec.processNoiseCov = mn2

    def rotateToWorld(self,m1):
        """ Rotates a marker from camera to world coordinate system with reference marker m1. """
        r_mat_1,_=cv2.Rodrigues(m1.rvec_cam)
        v=self.tvec_cam-m1.tvec_cam
        self.tvec_world=np.dot(v,r_mat_1)
        r_mat_2,_=cv2.Rodrigues(self.rvec_cam)
        r_mat_2_t=np.transpose(r_mat_2)
        self.rvec_world=np.dot(r_mat_2_t,r_mat_1)
        self.logger.debug("rotateToWorld: v: {0} r: {1}".format(v, self.rvec_world))
        self.rotateToWorldFiltered(m1)

    def distanceToMarkerXY(self,m):
        """ Computes distance to other marker m. """
        v1=m.tvec_world[0:2]
        v2=self.tvec_world[0:2]
        self.logger.debug("v1: {0}".format(v1))
        self.logger.debug("v2: {0}".format(v2))
        return np.linalg.norm(np.float32(v1)-np.float32(v2))

    def getAngleXY(self,m,v):
        """ Returns angle to other marker m. """
        # translate vector
        v1=self.tvec_world
        v2=m.tvec_world
        v_r_1=np.dot(np.transpose(self.rvec_world),v)
        t2=np.float32(v2)-np.float32(v1)
        try:
            angle=np.arctan2(np.cross(t2,v_r_1), np.dot(t2,v_r_1))
        except ValueError:
            traceback.print_exc()
            self.logger.warn("getAngle from {0} to {1}".format(self.id,m.id))
            self.logger.warn("self.rvec_world: {0}".format(self.rvec_world))
            self.logger.warn("v1: {0}".format(v1))
            self.logger.warn("v2: {0}".format(v2))
        return angle

    def rotateToWorldFiltered(self,m1):
        """ Rotates a marker from camera to world coordinate system with reference marker m1 bases on Kalman filtered positions"""
        try:
            r_mat_1,_=cv2.Rodrigues(np.float32(m1.rvec_cam_filter))
            v=np.float32(self.tvec_cam_filter)-np.float32(m1.tvec_cam_filter)
            self.tvec_world_filter=np.dot(v,r_mat_1)

            r_mat_2,_=cv2.Rodrigues(np.float32(self.rvec_cam_filter))
            r_mat_2_t=np.transpose(r_mat_2)
            self.rvec_world_filter=np.dot(r_mat_2_t,r_mat_1)

            r_mat_1,_=cv2.Rodrigues(m1.rvec_cam)
            v=self.tvec_cam-m1.tvec_cam
            self.tvec_world=np.dot(v,r_mat_1)

            r_mat_2,_=cv2.Rodrigues(self.rvec_cam)
            r_mat_2_t=np.transpose(r_mat_2)
            self.rvec_world=np.dot(r_mat_2_t,r_mat_1)
            self.logger.debug("rotateToWorld: v: {0} r: {1}".format(v, self.rvec_world_filter))
        except ValueError:
            traceback.print_exc()
            self.logger.warn("self.tvec_cam_filter {0}".format(self.tvec_cam_filter))
            self.logger.warn("m1.tvec_cam_filter: {0}".format(m1.tvec_cam_filter))
            self.logger.warn("self.rvec_cam_filter {0}".format(self.rvec_cam_filter))
            self.logger.warn("m1.rvec_cam_filter: {0}".format(m1.rvec_cam_filter))
            self.logger.warn("self.tvec_world_filter {0}".format(self.tvec_world_filter))
            self.logger.warn("self.rvec_world_filter {0}".format(self.rvec_world_filter))

    def distanceToMarkerXYFiltered(self,m):
        """ Computes distance to other marker m based on Kalman filtered positions."""
        v1=m.tvec_world_filter[0:2]
        v2=self.tvec_world_filter[0:2]
        return np.linalg.norm(np.float32(v1)-np.float32(v2))

    def getAngleXYFiltered(self,m,v):
        """ Returns angle to other marker m based on Kalman filtered positions."""
        try:
            v1=self.tvec_world_filter
            v2=m.tvec_world_filter
            v_r_1=np.dot(np.transpose(self.rvec_world_filter),v)
            t2=np.float32(v2)-np.float32(v1)
            angle=np.arctan2(np.cross(t2,v_r_1), np.dot(t2,v_r_1))
        except ValueError:
            self.logger.warn("getAngle from {0} to {1}".format(self.id,m.id))
            self.logger.warn("self.rvec_world: {0}".format(self.rvec_world_filter))
            self.logger.warn("self.tvec_world_filter: {0}".format(self.tvec_world_filter))
            self.logger.warn("v_r_1: {0}".format(v_r_1))
            self.logger.warn("angle: {0}".format(angle))
            self.logger.warn("v: {0}".format(v))
        return angle

    def __str__(self):
        """ Returns string representation. """
        return "id: {0} t_vec_cam: {1} r_vec_cam: {2} t_vec_world: {3} r_vec_world: {4}".format(self.id, self.tvec_cam, self.rvec_cam, self.tvec_world, self.rvec_world)

def main():
    """ main function for testing the class. """
    t1=np.float32([1,0,0])
    r1=np.float32([0,0,0])
    m1=MarkerPosition(t1,r1,1)
    t2=np.float32([0,-1,0])
    r2=np.float32([0,0,0])
    m2=MarkerPosition(t2,r2,2)
    m2.rotateToWorld(m1)
    print (m2.tvec_world)
    print (m2.rvec_world)
    print (m2.distanceToMarkerXY(m1))
    print (m2.getAngleXY(m1,[0,1,0])*180/np.pi)

if __name__ == "__main__":
    """Entry point, starts function main."""
    main()
