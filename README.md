# Hardware
- mBot from Makeblock
- Logitech C922

# Installation
This instruction assumes Ubuntu as operating system.

## Install required packages
sudo apt-get install libusb-1.0.0-dev libudev-dev

## Install OpenCV
Follow the steps described at http://www.pyimagesearch.com/2015/07/20/install-opencv-3-0-and-python-3-4-on-ubuntu/

## PID controller
Clone the project from https://github.com/ivmech/ivPID (ideally to ~/git).

## Install python-for-mbot
pip install pyserial cython hidapi
Clone the library https://gitlab.com/ralfluebben/python-for-mbot (ideally to ~/git), it is a copy of https://github.com/xeecos/python-for-mbot for Python3 compatibility.
For installation follow the steps described at https://github.com/xeecos/python-for-mbot

## Clone this project

# Configuration

## Connect the robot via bluetooth use
```
sudo rfcomm bind hci0 <mac-address> 1 ; sudo chmod a+rw /dev/rfcomm0
```
## Calibrate the camera
The camera can be calebrated with `opencv_interactive-calibration`, the results have to be updated in MarkerDetector.py

# Setup your environment (if you installed everything in ~/git/)
```
cd ~; source .profile;workon cv3; export PYTHONPATH=~/git/python-for-mbot/:~/git/ivPID/:$PTYHONPATH
```
# Start the robot
```
cd ~/git/robot/
python bot.py
```