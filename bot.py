#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import lib.mBot as mBot
import numpy as np
import time
import logging
import PID
import MarkerDetector
import traceback
import threading
import sys
import copy
import signal

class Bot:
    """ The class Bot implements an abstraction of the mBot robot with the task to montior a marker,
    if the marker is occupied by any object the robot drives to the object and pass over the marker to remove
    the object from the marker. This class also implements an emergency stop, if the robot approximates the marker and a
    object is between the robot and the marker, the robot stops and keeps a constant distance to the object.
    For details see the state machine in cleanUp().
    """

    def __init__(self,log_level=logging.INFO,show_image=True):
        self.log_level=log_level
        self.initLogger()
        self.bot = mBot.mBot()
        self.bot.startWithSerial("/dev/rfcomm0")
        self.max_speed=230
        self.min_speed=80
        self.distance_transition_approximate_realign=0.18 # [m]
        self.distance_transition_go_free_position=0.6 # [m]
        self.distance_transition_free_position_turn=0.5 #[m]
        self.angle_transmission=3 # [°]
        self.state=None
        self.marker_distance=0
        # PID for turn (1s ~ 90°)
        self.pid = PID.PID(0.007,0.001,0)
        self.pid.SetPoint=0.0
        self.pid.setSampleTime(0.1)

        #PID for moving forward
        self.pid_go = PID.PID(0.01,0.001,0)
        self.pid_go.SetPoint=0.0
        self.pid_go.setSampleTime(0.1)

        self.markerDetector=MarkerDetector.MarkerDetector(show_image=show_image)
        self.markerDetector.run()
        signal.signal(signal.SIGINT,self.exit)

        self.object_distance=0

        self.get_distance_running=False
        self.condition_distance=threading.Condition()
        self.emergency_distance=5 # [cm]

        #PID for distance control
        self.pid_dist = PID.PID(10,0.1,0)
        self.pid_dist.SetPoint=0
        # a offset is required since the motor starts turning at about 50
        self.pid_dist.offset=48
        self.pid_dist.setSampleTime(0.05)

        self.emergency_break=threading.Event()

    def emergency_stop(self):
        """ Returns True when an object is between the roboter and the marker minus the distance before the robot goes into the state realign """
        if (self.marker_distance-self.distance_transition_approximate_realign)*100 >= self.object_distance and self.object_distance <= self.emergency_distance:
            return True
        else:
            return False

    def emergency_stop_release(self):
        """ Returns True when the object between the robot and marker is removed."""
        ret=False
        self.condition_distance.acquire()
        if self.object_distance > (self.marker_distance-self.distance_transition_approximate_realign)*100:
            ret=True
        self.condition_distance.release()
        return ret

    def cbDistance(self,distance):
        """ Callback function executed when a distance is returned by the ultrasonic sensor.
            It updates the measured distance returned by the sensor, if an object is detected closely it signals an emergency break.
        """
        self.condition_distance.acquire()
        self.object_distance=distance
        self.condition_distance.notify()
        self.condition_distance.release()
        if self.emergency_stop():
            self.emergency_break.set()

    def getDistance(self):
        """ Requests a distance measurement from the ultrasonic sensor."""
        while self.get_distance_running==True:
            self.bot.requestUltrasonicSensor(1, 3, self.cbDistance)
            time.sleep(0.05) # we get the distance each 50 ms, the sensor supports 23 ms at minimum

    def keepDistance(self):
        """ The state for keeping a constant distance to an object. """
        self.get_distance_running=True
        threading.Thread(target=self.getDistance).start()
        while True:
            self.condition_distance.acquire()
            self.condition_distance.wait()
            dist=np.round(self.emergency_distance-self.object_distance, decimals=0)
            self.condition_distance.release()
            self.pid_dist.update(dist)
            if self.emergency_stop_release(): #Exit when the object is removed.
                break
            if dist != 0.0:
                speed=int(self.pid_dist.output+np.sign(self.pid_dist.output)*self.pid_dist.offset)
                if np.abs(speed) > self.max_speed:
                    speed=int(np.sign(self.pid_dist.output)*self.max_speed)
                self.goBySpeed(speed)
            else:
                self.stop()
        self.get_distance_running=False
        self.stop()

    def initLogger(self):
        """ Initializes our logger """
        # from https://docs.python.org/2/howto/logging.html#configuring-logging
        self.logger=logging.getLogger('Bot')
        self.logger.setLevel(self.log_level)
        ch = logging.StreamHandler()
        ch.setLevel(self.log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def stop(self):
        """ Stops the robot """
        self.bot.doMove(0,0)

    def exit(self,signal=None, frame=None):
        """ Cleans up on exit """
        self.markerDetector.stop()
        self.bot.exit(None,None)
        self.get_distance_running=False

    def turn(self, angle):
        """ Turns the robot.
        
        The turn is implemented by turning the motors in opposite directions. 
        Turning is realized by driving the motors by constant speed values for a duration correlated to the angle.
        Turning by time and constant speed is implemented since the speed control of the motor is very insensitive, e.g. small values
        below 50 do not rotate the motors at all, furthermore it depends on the charging level. 
        Using a constant speed at which the motors rotate definitely and control the angle by duration is much more precise.
        
        The duration to turn is calculated by a PI controller.
        """
        
        self.pid.update(angle/np.pi*180)
        move_time=np.abs(self.pid.output)
        self.logger.info("Turn by {0}° for {1}s".format(angle/np.pi*180, move_time))
        if angle < 0:
                self.logger.debug('Turn right')
                self.turnBackByTime(move_time)

        #go to left
        elif angle > 0:
            self.logger.debug('Turn left')
            self.turnByTime(move_time)

    def turnByTime(self, move_time):
        """ Turns for a duration of move_time by constant speed. """
        self.logger.debug("Turn for {0} s".format(move_time))
        self.bot.doMove(int(self.min_speed),-int(self.min_speed))
        time.sleep(move_time)
        self.stop()

    def turnBackByTime(self, move_time):
        """ Turns backward for a duration of move_time by constant speed. """
        self.logger.debug("Turn back for {0} s".format(move_time))
        self.bot.doMove(-int(self.min_speed),int(self.min_speed))
        time.sleep(move_time)
        self.stop()

    def goForward(self,dist,dc=0):
        """ Moves forward. The distance dist is translated to a duration for which the robot moves. 
        For a detailed description why distance is translated to a duration and not to speed, see method turn()
        
        If dc > 0, a distance control is actived so that the robot stops when a object is close to the robot.
        """
        self.logger.debug('goForward')
        self.pid_go.update(dist*100)
        move_time=np.abs(self.pid_go.output)
        self.logger.info("Go forward by {0} cm for {1}s".format(dist*100, move_time))
        self.goForwardByTime(move_time,dc)

    def goBackward(self,dist,dc=0):
        """ Moves backward. The distance dist is translated to a duration for which the robot moves. 
        For a detailed description why distance is translated to a duration and not to speed, see method turn()
        """
        self.logger.debug('go_backward')
        self.pid_go.update(dist)
        move_time=np.abs(self.pid_go.output)
        self.goBackwardByTime(move_time,dc)

    def goForwardByTime(self, move_time,dc):
        """Moves forward for duration move_time"""
        self.logger.debug('goForwardByTime: {0}'.format(move_time))
        self.goByTime(move_time,1,dc)

    def goBackwardByTime(self, move_time,dc):
        """Moves backward for duration move_time"""
        self.logger.debug('goBackwardByTime: {0}'.format(move_time))
        self.goByTime(move_time,-1,dc)

    def goByTime(self, move_time, direction, dc):
        """Moves forward or backward for duration move_time"""
        self.logger.info('for move  {0}, dir {1}'.format(move_time,direction))
        self.bot.doMove(direction*self.min_speed,direction*self.min_speed)
        if dc==0:
            time.sleep(move_time)
        else:
            self.get_distance_running=True
            self.emergency_break.clear()
            threading.Thread(target=self.getDistance).start()
            self.emergency_break.wait(move_time)
            self.get_distance_running=False
        self.stop()

    def goBySpeed(self, speed,direction=1):
        """ Moves forward by speed """
        self.logger.info('move by  {0}, dir {1}'.format(speed,direction))
        self.bot.doMove(direction*speed,direction*speed)

    def getRelativePosXYFiltered(self,id1,id2):
        """ Returns the dist and angle between to markers. """
        dist,angle=self.markerDetector.getRelativePosXYFiltered(id1,id2)
        return [dist,angle]

    def waiting(self):
        """ Initial state when the marker with id 3 is free.
        When it is detected as occupied for continuous measurements, the state cleanUp is entered.
        """
        self.marker_distance=None
        while self.marker_distance is None:
            self.markerDetector.condition_marker.acquire()
            _dist,_angle=self.getRelativePosXYFiltered(1,3)
            self.marker_distance=copy.deepcopy(_dist)
            angle=copy.deepcopy(_angle)
            self.markerDetector.condition_marker.release()
            if self.marker_distance is None or angle is None:
                continue

        self.logger.info("Waiting")
        while True:
            self.markerDetector.condition_marker.acquire()
            self.markerDetector.condition_marker.wait()
            markers=self.markerDetector.getMarker()
            if 2 in markers:
                if markers[3].not_detected>10: # threshold for continuously not detected
                    self.markerDetector.condition_marker.release()
                    self.cleanUp()
                else:
                    self.markerDetector.condition_marker.release()
                    self.logger.info("Marker 2 is free.")
                    time.sleep(0.5)
            else:
                self.markerDetector.condition_marker.release()
                self.logger.info("Waiting for markers.")
                time.sleep(1)

    def cleanUp(self):
        """ State cleanUP, implements sub states
        * align: aligns the robot in the direction of marker 3
        * approximate: go foward in the direction of the marker, stops close the marker
        * realign: realign in the direction of the marker to reduce error of angle
        * go: move forward until distance to marker is large
        * free_position_back: move backward to release the object
        *free_position_turn: align in the direction of the marker
        * emergency_stop: is entered when object is detected in state approximate
        """
        self.state="align"
        while self.state is not "done":
            self.logger.info('State: {0}'.format(self.state))
            self.markerDetector.condition_marker.acquire()
            self.markerDetector.condition_marker.wait()
            _dist,_angle=self.getRelativePosXYFiltered(1,3)
            self.marker_distance=copy.deepcopy(_dist)
            angle=copy.deepcopy(_angle)
            self.markerDetector.condition_marker.release()
            if self.marker_distance is None or angle is None:
                time.sleep(1)
                continue

            self.logger.info("Dist: {0} Angle: {1}".format(self.marker_distance*100,angle/np.pi*180))

            if self.state=="align":
                #turn until angle is about zero
                if np.abs(angle*180/np.pi) < self.angle_transmission:
                    self.state="approximate"
                    continue
                self.turn(angle)
                #self.stop()
            elif self.state=="approximate":
                if self.marker_distance < self.distance_transition_approximate_realign:
                    self.state="realign"
                    continue
                if self.emergency_stop():
                    self.state="emergency_stop"
                    continue
                dist=self.marker_distance-self.distance_transition_approximate_realign
                self.logger.info('State: Go for {0}'.format(dist*100))
                self.goForward(dist,self.emergency_distance)

            elif self.state=="realign":
                #turn until angle is about zero
                if np.abs(angle*180/np.pi) < self.angle_transmission:
                    self.state="go"
                    continue
                self.turn(angle)

            elif self.state=="go":
                if self.marker_distance > self.distance_transition_go_free_position:
                    self.state="free_position_back"
                    continue
                else:
                    dist=self.marker_distance-self.distance_transition_go_free_position
                    self.goForward(dist)
            elif self.state=="free_position_back":
                #turn until angle is about zero
                if self.marker_distance < self.distance_transition_free_position_turn:
                    self.state="free_position_turn"
                    continue
                else:
                    dist=np.abs(self.distance_transition_free_position_turn-self.marker_distance)
                    self.goBackward(dist)
            elif self.state=="free_position_turn":
                #turn until angle is about zero
                if np.abs(angle*180/np.pi) < self.angle_transmission:
                    self.state="done"
                    continue
                self.turn(angle)
            elif self.state=="emergency_stop":
                self.keepDistance()
                self.state="approximate"
            else:
                self.stop()
            self.stop()

def main_distance():
    """ Alternative main function to test keep.Distance """
    b=Bot()
    try:
        b.stop()
        b.keepDistance()
        b.stop()
    except KeyboardInterrupt:
        print("Exit")
    except SystemExit:
        print ("Bot exit")
    except :
        print ("Error")
        traceback.print_exc()
    b.stop()
    b.exit()

def main():
    """ Main function to start the robot """
    b=Bot()
    try:
        b.stop()
        b.waiting()
        b.stop()
    except KeyboardInterrupt:
        print("Exit")
    except SystemExit:
        print ("Bot exit")
    except :
        print ("Error")
        traceback.print_exc()
    b.stop()
    b.exit()

if __name__ == "__main__":
    """ Entry point, it starts the main() function """
    #main_distance()
    main()

