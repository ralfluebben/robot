#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#http://docs.opencv.org/3.1.0/d9/d6a/group__aruco.html#ga16fda651a4e6a8f5747a85cbb6b400a2
import numpy as np
import cv2
import cv2.aruco as aruco
import utils.webcam
import logging
import MarkerPosition
import threading

class MarkerDetector:
    """ This class detects various aruco markers from a webcam image. 
    The positions of the markers are identified. The marker with id 2 is used as a reference to create a world coordinate system.
    The distances and angles between other markers are calculated based on this world coordinate system.
    Camera matrix and distortion matrix have to be updated here when other/new cameras are used.
    The detection runs in an own thread that synchronizes with the webcam.
    Internally, it manages objects for each marker.
    The marker ids are: 
    - id 1 This is the robot.
    - id 2 This is the reference for the world coordinate system.
    - id 3 This is the marker which is monitored if it is occupied by an object.
    """
    
    
    def __init__(self,log_level=logging.INFO,show_image=False):
         """ Initialzes the object, sets the camera matrix and distortion matrix, starts the webcam. """
        self.log_level=log_level
        self.initLogger()
        self.show_image=show_image
        self.ref_marker=2
        # camera matrix and distortion matrix 
        self.camera="C922"
        if self.camera=="C922":
            self.mtx=np.float32([[9.2485885589311249e+02, 0., 6.2623330699763221e+02],[0., 9.2485885589311249e+02, 3.7356100395939399e+02],[ 0., 0., 1.]]).reshape(3,3)
            self.dist=np.float32([1.2345344084259544e-01, -2.6557097777991207e-01, 0., 0., 1.1302998754331675e-01]).reshape(-1,5)
        elif self.camera=="PiCamera":
            self.mtx=np.float32([[9.3379166915375345e+02, 0., 6.2135730227593103e+02],[0., 9.3379166915375345e+02, 3.7414485765135822e+02],[ 0., 0., 1.]]).reshape(3,3)
            self.dist=np.float32([2.2664050259358229e-01, -5.4773069450293921e-01, 0., 0., 3.5323107043811142e-01]).reshape(-1,5)

        # start webcam
        self.webcam = utils.webcam.Webcam(log_level=self.log_level,camera=self.camera)
        self.webcam.start()

        # init aruco
        self.aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
        self.parameters =  aruco.DetectorParameters_create()
        self.font = cv2.FONT_HERSHEY_SIMPLEX #font for displaying text (below)

        self.marker_length=0.06
        self.marker={}
        self.initial_position_all_markers=False
        self.condition_marker=threading.Condition()
        self.stop_event=threading.Event()

    def initLogger(self):
        """ Initializes the logger. """
        # from https://docs.python.org/2/howto/logging.html#configuring-logging
        self.logger=logging.getLogger('MarkerDetector')
        self.logger.setLevel(self.log_level)
        ch = logging.StreamHandler()
        ch.setLevel(self.log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def stop(self):
        """ Cleans up on exit. """
        self.stop_event.set()
        self.webcam.stop()

    def _detectMarker(self):
        """ Detects the markers from webcam images. """
        # Get image
        self.logger.debug("Start marker detection")
        while True:
            #logging.debug("Marker detection")
            self.logger.debug("Waiting New frame")
            self.webcam.frame_event.wait()
            self.logger.debug("New frame")
            if self.stop_event.is_set():
                break;
            frame=self.webcam.get_current_frame()

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            corners, ids, _ = aruco.detectMarkers(gray, self.aruco_dict, parameters=self.parameters)
            self.condition_marker.acquire()
            rvecs, tvecs = aruco.estimatePoseSingleMarkers(corners, self.marker_length, self.mtx, self.dist)

            if ids is not None:
                i=0
                for idx in ids:
                    idx=idx[0]
                    rvec=rvecs[i][0]
                    tvec=tvecs[i][0]
                    if self.show_image==True:
                        aruco.drawAxis(frame, self.mtx, self.dist, rvec, tvec, 0.11)
                    if idx in self.marker and self.ref_marker in self.marker:
                        self.marker[idx].update(tvec,rvec)
                        self.marker[idx].rotateToWorld(self.marker[self.ref_marker])
                    else:
                        self.logger.debug("New marker with id {0}".format(idx))
                        self.marker[idx]=MarkerPosition.MarkerPosition(tvec,rvec,idx)
                    i=i+1

            if self.show_image==True:
                cv2.imshow('frame',cv2.resize(frame, (0,0), fx=1, fy=1))
                cv2.waitKey(50)

            if self.initial_position_all_markers==False and 3 in self.marker and 2 in self.marker and 1 in self.marker:
                # We have an initial position for all markers
                self.initial_position_all_markers=True
                self.marker[1].rotateToWorld(self.marker[self.ref_marker])
                self.marker[2].rotateToWorld(self.marker[self.ref_marker])
                self.marker[3].rotateToWorld(self.marker[self.ref_marker])

            # marker 2 and 3 are static markers, so we can use the recent values
            # marker 1 is the robot and may move, so we have to reset the marker when cannot detect it
            if ids is not None and self.initial_position_all_markers==True:
                if 3 in ids:
                    self.marker[3].rotateToWorld(self.marker[self.ref_marker])
                    self.marker[3].not_detected=0
                else:
                    self.marker[3].not_detected+=1
                if 2 in ids:
                    self.marker[2].rotateToWorld(self.marker[self.ref_marker])
                    self.marker[2].not_detected=0
                else:
                    self.marker[2].not_detected+=1
                if 1 in ids:
                    self.marker[1].rotateToWorld(self.marker[self.ref_marker])
                    self.marker[1].not_detected=0
                else:
                    #self.marker[1]=None # We have to reset marker one, since it moves
                    self.marker[1].not_detected+=1

            if ids is None and self.initial_position_all_markers==True:
                # We have to reset marker one, since it moves
                self.marker[1].not_detected+=1
                self.marker[2].not_detected+=1
                self.marker[3].not_detected+=1
            self.condition_marker.notify_all()
            self.condition_marker.release()

        self.logger.debug("Stop marker detection")

    def getDistanceXY(self,id1,id2):
        """ Returns the distance between the markers with ids id1 and id2 """
        if id1 in self.marker and id2 in self.marker:
            return self.marker[id1].distanceToMarkerXY(self.marker[id2])
        else:
            return None

    def getAngleXY(self,id1,id2):
        """ Returns the angle between the markers with ids id1 and id2 """
        if id1 in self.marker and id2 in self.marker:
            return self.marker[id1].getAngleXY(self.marker[id2],[0,1,0])[2]
        else:
            return None

    def getRelativePosXY(self,id1,id2):
        """ Returns the distance and angle between the markers with ids id1 and id2 """
        return [self.getDistanceXY(id1,id2),self.getAngleXY(id1, id2)]

    def getDistanceXYFiltered(self,id1,id2):
        """ Returns the distance between the markers with ids id1 and id2 filtered by a Kalman filter."""
        if id1 in self.marker and id2 in self.marker and self.ref_marker in self.marker:
            return self.marker[id1].distanceToMarkerXYFiltered(self.marker[id2])
        else:
            return None

    def getAngleXYFiltered(self,id1,id2):
        """ Returns the angle between the markers with ids id1 and id2 filtered by a Kalman filter."""
        if id1 in self.marker and id2 in self.marker and self.ref_marker in self.marker:
            return self.marker[id1].getAngleXYFiltered(self.marker[id2],[0,1,0])[2]
        else:
            return None

    def getRelativePosXYFiltered(self,id1,id2):
        """ Returns the distance and angle between the markers with ids id1 and id2 filtered by a Kalman filter."""
        return [self.getDistanceXYFiltered(id1,id2),self.getAngleXYFiltered(id1, id2)]

    def run(self):
        """ Starts the thread for marker detection. """
        self.thread=threading.Thread(target=self._detectMarker, args=()).start()

    def getMarker(self):
        """ Returns the marker object. """
        return self.marker

def main():
    """ Main function for testing this class. """
    m=MarkerDetector(log_level=logging.DEBUG,show_image=True)
    m.run()
    measurement_dist=[]
    prediction_dist=[]
    measurement_angle=[]
    prediction_angle=[]
    p=100
    try:
        while p>0:
            m.condition_marker.acquire()
            m.condition_marker.wait()
            marker=m.getMarker()
            if 3 in marker and 2 in marker and 1 in marker:
                dist,angle=m.getRelativePosXY(1,3)
                m.logger.debug("Distance[cm]: {0}, Angle[°]: {1}".format(dist*100,angle/np.pi*180))
                measurement_dist.append(dist*100)
                measurement_angle.append(angle/np.pi*180)

                dist,angle=m.getRelativePosXYFiltered(1,3)
                #m.logger.debug("FIL: Distance[cm]: {0}, Angle[°]: {1}".format(dist*100,angle/np.pi*180))
                prediction_dist.append(dist*100)
                prediction_angle.append(angle/np.pi*180)
                p=p-0.1
            else:
                m.logger.debug("Keys in marker: {0}".format(marker.keys()))
            m.condition_marker.release()
    except KeyboardInterrupt:
        m.logger.debug("Stop detection")
    except:
        traceback.print_exc()
    m.stop()

    # Two subplots for distance and angle evaluation
    #import matplotlib.pyplot as plt
    #_, axarr = plt.subplots(2, sharex=True)
    #h1,=axarr[0].plot(measurement_dist,label='measurement')
    #h2,=axarr[0].plot(prediction_dist,label='prediction')
    #h3,=axarr[1].plot(measurement_angle,label='measurement')
    #h4,=axarr[1].plot(prediction_angle,label='prediction')
    #plt.legend(handles=[h1,h2,h3,h4])
    #plt.show()

if __name__ == "__main__":
    """ Entry point, starts main() """
    # import modules we only need in main()
    import traceback
    import time
    main()
