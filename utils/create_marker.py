#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cv2
import cv2.aruco as aruco

"""Script to write marker to an image for printing. """

for idx in range(1,4):
    img=aruco.drawMarker(aruco.Dictionary_get(aruco.DICT_4X4_50), idx, 500)
    marker_filename="images/marker_"+str(idx)+".png"
    cv2.imshow(marker_filename, img)
    cv2.waitKey(0)
    cv2.imwrite(marker_filename,img)
