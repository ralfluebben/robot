#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#It is based on https://rdmilligan.wordpress.com/2015/06/28/opencv-camera-calibration-and-pose-estimation-using-python/
import cv2
from threading import Thread, Event
import os
import time
import logging

class Webcam:
    """ Class for the webcam, it continuously reads frames from a camera. Reading is implemented in a thread to always have the
    latest frame available. Autofocus is turned off, frame height and width is fixed, so the camera properties do not change.
    """

    def __init__(self,log_level=logging.DEBUG,camera="C922"):
        """ Initializes the webcam object and sets camera properties. """
        self.log_level=log_level
        self.initLogger()
        self.thread = None
        self._is_running = True
        self.stop_event=Event()
        self.camera=camera
        self.current_frame_number=0
        self.frame_event=Event()
        if self.camera=="C922":
            self.video_capture = cv2.VideoCapture(1)
            self.video_capture.set(cv2.CAP_PROP_AUTOFOCUS, 0)
            self.video_capture.set(cv2.CAP_PROP_FRAME_WIDTH,1280);
            self.video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT,720);
            self.video_capture.set(cv2.CAP_PROP_FOCUS,0);
            self.current_frame = self.video_capture.read()[1]
        elif self.camera=="PiCamera":
            # modules only available pn RPi
            from picamera.array import PiRGBArray
            from picamera import PiCamera
            self.camera = PiCamera()
            #res = (640,480)
            res = (1296,976)
            #res=(2592,1944)
            self.camera.start_preview()
            time.sleep(4)
            self.camera.stop_preview()
            self.camera.resolution = res
            self.camera.framerate = 30
            self.video_capture = PiRGBArray(self.camera, size=res)
            self.current_frame = None
        else:
            self.logger.error("No camera sepecified.")
            raise SystemExit

    def initLogger(self):
        """ Initializes the logger. """
        # from https://docs.python.org/2/howto/logging.html#configuring-logging
        self.logger=logging.getLogger('Webcam')
        self.logger.setLevel(self.log_level)
        ch = logging.StreamHandler()
        ch.setLevel(self.log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def start(self):
        """ Starts the thread for reading frames."""
        if self.camera=="C922":
            self.thread=Thread(target=self._update_frame, args=()).start()
        elif self.camera=="PiCamera":
            self.thread=Thread(target=self._update_frame_pi, args=()).start()

    def stop(self):
        """Cleans up the camera object on exit."""
        self.stop_event.set()
        if self.thread is not None:
            self.thread.join()
        if self.camera=="C922":
            self.video_capture.release()
        elif self.camera=="PiCamera":
            self.camera.close()

    def _update_frame(self):
        """ Updates the frame for opencv compatible camera. """
        while not self.stop_event.is_set():
            self.current_frame = self.video_capture.read()[1]
            self.current_frame_number+=1
            self.frame_event.set()
        self.logger.debug("exit _update_frame")
            
    def _update_frame_pi(self):
        """ Updates the frame from a raspberry pi camera. """
        for frame in self.camera.capture_continuous(self.video_capture, format="bgr", use_video_port=True):
            if self.stop_event.is_set():
                self.logger.debug("exit _update_frame_pi")
                break
            self.current_frame = frame.array
            self.video_capture.seek(0)
            self.current_frame_number+=1
            self.frame_event.set()
            #self.video_capture.truncate(0)

    # get the current frame
    def get_current_frame(self):
        """ Returns the current frame. """
        return self.current_frame

    def show_current_frame(self):
        """ Shows the current frame. """
        if self.current_frame is not None:
            cv2.imshow('frame', cv2.resize(self.current_frame, (0,0), fx=0.5, fy=0.5))
            cv2.waitKey(500)

def main():
    """ main function for testing this class. """
    cam=Webcam()
    cam.start()
    while True:
        cam.show_current_frame()
            
if __name__ == "__main__":
    """ Entry point that starts main(). """
    main()
