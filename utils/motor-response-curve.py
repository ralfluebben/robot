#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This file is part of robot.
# Copyright (C) 2017 Ralf Lübben <ralf.luebben@gmail.com>
#
# It is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bot
import numpy as np
from numpy import (array, dot, arccos, clip)
from numpy.linalg import norm
import time
import logging
import matplotlib.pyplot as plt

""" Helper script to empirically estimate the relation between move_time and hence angle or distance, respectively. """

myBot = bot.Bot(show_image=False)
angle_diff={}
time_steps=[10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,50,60,70,80,90,100,110,120,200,300,400,500,600,700,800,1000,2000]
#time_steps=[120,200]
results=np.empty([2, len(time_steps)])

c=0
for t in time_steps:
    print ("Time steep: {0}".format(t))
    time.sleep(2)
    current_angle=[]
    i=10
    marker=myBot.markerDetector.getMarker()
    while i>0:
        print ("Get angle trial: {0}".format(i))
        id1=1
        id2=3
        marker=myBot.markerDetector.getMarker()
        dist,a=myBot.markerDetector.getRelativePosXYFiltered(id1,id2)
        while a is None:
            for m in marker:
                print (type(m))
            dist,a=myBot.markerDetector.getRelativePosXYFiltered(id1,id2)
            print (a)
            time.sleep(1)
            continue
        print ("Dist: {0}".format(dist))
        print ("Angle: {0}".format(a))
        current_angle.append(a)
        i=i-1
        time.sleep(0.15)
    myBot.turn_by_time(t/1000.0)
    new_angle=[]
    i=10
    while i>0:
        print ("Get angle trial: {0}".format(i))
        id1=1
        id2=3
        dist,a=myBot.markerDetector.getRelativePosXYFiltered(id1,id2)
        while a is None:
            for m in marker:
                print (type(m))
            #print a
            dist,a=myBot.markerDetector.getRelativePosXYFiltered(id1,id2)
            #print a
            time.sleep(1)
            continue
        new_angle.append(a)
        i=i-1
        time.sleep(0.15)
    myBot.turn_back_by_time(t/1000.0)
    results[0][c]=t
    results[1][c]=np.mean(np.array(current_angle))-np.mean(np.array(new_angle))
    angle_diff[t]={"mean" : np.mean(np.array(current_angle))-np.mean(np.array(new_angle))}
    c=c+1

print ("Sort")
x1=[]
y1=[]
for key in sorted(angle_diff.keys()):
    #print ("%s: %s" % (key, dist_diff[key]))
    x1.append(key)
    mea=angle_diff[key]
    y1.append(mea['mean']*180/np.pi)

print ("Plot")
print (x1)
print (y1)
plt.plot(x1,y1)
plt.ylabel('Turn by [°]')
plt.xlabel('time [ms]')
plt.show()

# measurement distance
dist_diff={}
dist_results=np.empty([2, len(time_steps)])
c=0
for t in time_steps:
    print ("Time steep: {0}".format(t))
    time.sleep(2)
    current_dist=[]
    i=10
    while i>0:
        print ("Get dist trial: {0}".format(i))
        id1=1
        id2=3
        marker=myBot.markerDetector.getMarker()
        while marker[1] is None:
            print ("No marker 1")
            time.sleep(1)
            continue
        pos=marker[1].tvec_world_filter[1]
        current_dist.append(pos)
        i=i-1
        time.sleep(0.15)
    myBot.go_forward_by_time(t/1000.0,0)
    new_dist=[]
    i=10
    while i>0:
        print ("Get dist trial: {0}".format(i))
        id1=1
        id2=3
        marker=myBot.markerDetector.getMarker()
        while marker[1] is None:
            print ("No marker 1")
            time.sleep(1)
            continue
        pos=marker[1].tvec_world[1]
        new_dist.append(pos)
        i=i-1
        time.sleep(0.15)
    myBot.go_backward_by_time(t/1000.0,0)
    dist_results[0][c]=t
    dist_results[1][c]=np.mean(np.array(current_dist))-np.mean(np.array(new_dist))
    dist_diff[t]={"mean" : np.mean(np.array(current_dist))-np.mean(np.array(new_dist))}
    c=c+1

x2=[]
y2=[]
for key in sorted(dist_diff.keys()):
    print ("%s: %s" % (key, dist_diff[key]))
    x2.append(key)
    mea=dist_diff[key]
    y2.append(mea['mean']*100)
print (x2)
print (y2)
plt.plot(x2,y2)
plt.ylabel('Go by [cm]')
plt.xlabel('time [ms]')
plt.show()
myBot.stop()
myBot.exit()
